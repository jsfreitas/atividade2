/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atividade.segunda.bancodedados.restapi;

import atividade.segunda.bancodedados.dao.PessoaDAO;
import java.util.List;

import atividade.segunda.bancodedados.dao.PessoaPeloID;
import atividade.segunda.bancodedados.domain.Pessoa;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author WilliamFernandoMende
 */
@RestController
public class PessoaApi {

    private PessoaDAO pessoaDAO = new PessoaDAO();
    @GetMapping("/pessoas")
    public List<Pessoa> obterPessoas() {
        return pessoaDAO.findAll();
    }
    private PessoaPeloID pessoaPeloID = new PessoaPeloID();
    @RequestMapping(value = "id/{numero}", method = RequestMethod.GET)
    public PessoaPeloID obterPeloID(@PathVariable(value = "numero") Integer numero) throws Exception {
            return new PessoaPeloID();
        }

    }



